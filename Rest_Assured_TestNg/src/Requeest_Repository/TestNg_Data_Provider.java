package Requeest_Repository;

import org.testng.annotations.DataProvider;

public class TestNg_Data_Provider {
	
	@DataProvider()
	public Object[][] Post_Testng_DataProvider(){
		return new Object[][]
				{
					{"morpheus","leader"},
					{"Akash","QA"},
					{"Mohan","Manager"}
				};
	}

}
