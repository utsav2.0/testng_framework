package Testing_Pakage;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_methods.common_method_handle_API;
import Endpoint.patch_endpoint;
import Requeest_Repository.Patch_request_repository;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;
import io.restassured.path.json.JsonPath;

public class Patch_Test_Case extends common_method_handle_API {
	static File log_dir;
	static String requestbody;
	static String endpoint;
	static String responsebody;

	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = Handle_Directory.create_log_directory("Patch_TestCase_logs");
		requestbody = Patch_request_repository.patch_request_tc1();
		endpoint = patch_endpoint.patch_endpont_TC1();
	}

	@Test
	public static void Patch_executor() throws IOException {

		for (int i = 0; i < 5; i++) {
			int statuscode = patch_statuscode(requestbody, endpoint);
			System.out.println(statuscode);

			if (statuscode == 200) {
				responsebody = patch_responsebody(requestbody, endpoint);
				System.out.println(responsebody);

				Patch_Test_Case.validator(requestbody, responsebody);
				break;
			} else {
				System.out.println("PATCH Expected status code not found hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);

	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String testclassname = Patch_Test_Case.class.getName();
		Handle_Api_logs.evidence_creator(log_dir, testclassname, endpoint, requestbody, responsebody);

	}

}
