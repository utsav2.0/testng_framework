package Testing_Pakage;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_methods.common_method_handle_API;
import Endpoint.get_endpoint;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;

public class Get_Test_Case extends common_method_handle_API {
	static File log_dir;
	static String endpoint;
	static String responsebody;

	@BeforeTest
	public static void Test_Start() {
		log_dir = Handle_Directory.create_log_directory("Get_TestCase_Logs");
		endpoint = get_endpoint.get_endpoint_tc1();
	}

	@Test
	public static void Get_executor() throws IOException {

		for (int i = 0; i < 5; i++) {
			int statuscode = get_statuscode(endpoint);
			System.out.println(statuscode);

			if (statuscode == 200) {
				String responsebody = get_responsebody(endpoint);
				System.out.println(responsebody);

				Get_Test_Case.get_validator(responsebody);
				break;
			} else {
				System.out.println("Expected statuscode not found hence retrying ");
			}
		}
	}

	public static void get_validator(String responsebody) {
		int excp_id[] = { 1, 2, 3, 4, 5, 6 };
		String excp_email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		String excp_fname[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String excp_lname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

		JSONObject res_array = new JSONObject(responsebody);
		JSONArray data_array = res_array.getJSONArray("data");
		int count = data_array.length();

		for (int i = 0; i < count; i++) {
			int id = excp_id[i];
			String email = excp_email[i];
			String fname = excp_fname[i];
			String lname = excp_lname[i];

			int res_id = data_array.getJSONObject(i).getInt("id");
			String res_email = data_array.getJSONObject(i).getString("email");
			String res_fname = data_array.getJSONObject(i).getString("first_name");
			String res_lname = data_array.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, id);
			Assert.assertEquals(res_email, email);
			Assert.assertEquals(res_fname, fname);
			Assert.assertEquals(res_lname, lname);
		}

	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String testclassname = Get_Test_Case.class.getName();
		Handle_Api_logs.evidence_creator(log_dir, testclassname, endpoint, null, responsebody);
	}
}
