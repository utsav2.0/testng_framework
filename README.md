# Testng_Framework

The TestNG framework I have developed comprises six main components, each serving a distinct purpose:

1.	Test Driving Mechanism:
•	Utilizes the Testing.xml configuration file to orchestrate test execution through the use of the @Test annotation applied to each test script.

2.	Test Scripts:
•	i have all the testcase specific test scripts. for each API we have multiple Test cases and for each test case we have one single test script.

3.	Common Functions:
•	Categorized into two sections:
•	a) API-Related Common Functions: Functions facilitating API interaction, including triggering the API, retrieving the status code, and fetching the response body.
•	b) Utilities: Comprising utilities such as:
•	i. Automatic creation of directories during test execution. If a directory exists, it exists delete it and create new one.
•	ii. Once Api is Triggered logging API details, including endpoint, request body, response body, and headers, into separate text files for each test script.
•	iii. Reading data from an Excel file using Apache-POI libraries.

4.	Data/Variable File:
•	Involves reading and managing data from Excel files, implemented through the use of Apache-POI libraries.

5.	Libraries:
•	Encompasses the following libraries:
•	a) Rest Assured: Utilized for triggering APIs, extracting status codes, extracting responsebody.
•	b) JsonPath: To parse requestbody.
•	c) TestNG: Used for validating response body parameters through assertions.
•	d) Apache-POI: Employed for reading and writing data to Excel files.

6.	Reports:
•	Implements a reporting mechanism using Extent-Report to generate comprehensive test execution reports.

This framework architecture ensures a systematic and modular approach to API testing, facilitating efficient test design, execution, and reporting.


